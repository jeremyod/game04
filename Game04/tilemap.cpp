#include "stdafx.h"
#include "tilemap.h"

void TileMap::draw(sf::RenderTarget& target) const
{
    // apply the transform
    //states.transform *= getTransform();

    // apply the tileset texture
    //states.texture = &m_tileset;

    // draw the vertex array
    target.draw(m_vertices);
}

bool TileMap::load(const std::string& tileset, sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height)
{
    /*
     * tileset = string, filename
     * tileSize = vector2u() size of each tile in tileset
     * tiles = int array that lays out which tiles where
     * width = the total width to layout the tiles from "tiles"
     * height = the total height to layout the tiles from "tiles"
     *
     * ex. load("tileset.png", sf::Vector2u(32, 32), level, 32, 16);
     */

     // load the tileset texture
    if (!this->m_tileset.loadFromFile(tileset))
        return false;

    // resize the vertex array to fit the level size
    this->m_vertices.setPrimitiveType(sf::Quads);
    this->m_vertices.resize(width * height * 4);

    // populate the vertex array, with one quad per tile
    for (unsigned int i = 0; i < width; ++i)
        for (unsigned int j = 0; j < height; ++j)
        {
            // get the current tile number
            int tileNumber = tiles[i + j * width];

            // find its position in the tileset texture
            int tu = tileNumber % (this->m_tileset.getSize().x / tileSize.x);
            int tv = tileNumber / (this->m_tileset.getSize().x / tileSize.x);

            // get a pointer to the current tile's quad
            sf::Vertex* quad = &m_vertices[(i + j * width) * 4];

            // define its 4 corners
            quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
            quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
            quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
            quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

            // define its 4 texture coordinates
            quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
            quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
            quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
            quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
        }

    return true;
}

sf::VertexArray TileMap::getVertices()
{
    return this->m_vertices;
}
sf::Texture TileMap::getTexture()
{
    return this->m_tileset;
}
int TileMap::translatePos()
{
    // Translate the position of a texture
    // https://www.programiz.com/cpp-programming/vectors
    // vector<int> num {1, 2, 3, 4, 5};
    //  num.at(0) 
}
