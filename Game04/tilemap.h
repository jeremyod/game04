#pragma once
#include "stdafx.h"

class TileMap
{

private:
    virtual void draw(sf::RenderTarget& target) const;
    sf::VertexArray m_vertices;
    sf::Texture m_tileset;

public:
    bool load(const std::string& tileset, sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height);
    sf::VertexArray getVertices();
    sf::Texture getTexture();
    int translatePos();

};