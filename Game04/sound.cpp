#include "sound.h"


GameSound::GameSound()
{
	this->soundInit();
}
void GameSound::soundInit()
{
	this->gunFireBuffer.loadFromFile("res/snd/blaster_med_child_04-2.wav");
	this->gunFire.setBuffer(this->gunFireBuffer);

	this->projectileHitBuffer.loadFromFile("res/snd/projectile_small_met_01.wav");
	this->projectileHit.setBuffer(this->projectileHitBuffer);

	this->menuSelectBuffer.loadFromFile("res/snd/mouseover.wav");
	this->menuSelectSound.setBuffer(this->menuSelectBuffer);
}
void GameSound::play_gunfire()
{
	std::cout << "[Sound] Player Gunfire\n";
	this->gunFire.play();
}
void GameSound::play_projectilehit()
{
	std::cout << "[Sound] Player Projectile Hit\n";

	this->projectileHit.play();
}
void GameSound::play_menu_select()
{
	// Play sound on menu selection
	std::cout << "[Sound] Menu Select\n";
	this->menuSelectSound.play();
}