#include <iostream>
#include "game.h"

int main()
{
	std::srand(static_cast<unsigned>(time(NULL)));

	// Init engine
	Game game;

	while (game.running())
	{
		// update
		game.update();

		// game render
		game.render();

	}

	return 0;
}