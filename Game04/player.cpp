#include "game.h"

/*
*  Player related stuff here
*
*/


void Game::player_move(sf::Keyboard::Key direction)
{
	// scope error an sfml bug?
	// https://stackoverflow.com/questions/63610623/warning-c26812-enum-type-is-unscoped-prefer-enum-class-over-enum

	// We movin!

	// getCenter: 256,128

	if (direction == sf::Keyboard::D)
	{
		// We're going right!
		//std::cout << "[RIGHT][Center: " << this->fixedView.getCenter().x << "," << this->fixedView.getCenter().y << "][PlayerPOS: " << this->playerSprite.getPosition().x << "," << this->playerSprite.getPosition().y << "]\n";

		if ((this->playerSprite.getScale().x < 0) || (this->playerSprite.getRotation() == 180))
		{
			/*
			 * Insure that the character sprite is facing the correct direction
			 */
			// the X plane (width), facing Right
			this->updatePlayerScale('x', 'R');
		}

		if ((this->fixedView.getCenter().x + 256) >= 1024)
		{
			/*
			 * We have reached the right-side of the map
			 * and cant move the map (view) any further.
			 */

			if ((this->playerSprite.getPosition().x + 64) >= 1024)
			{
				/*
				 * The character has now reached the right-side end of the map
				 * and can not move any further.
				 */
				std::cout << "Player can not move any further!\n";
			}
			else {
				/*
				 * The map can not move any further right.
				 * However, the character can still move a bit further
				 */
				this->playerSprite.move(4.f, 0.f);
				this->updatePlayerMovement('x');
			}
		}
		else {
			if (this->playerSprite.getPosition().x != (this->fixedView.getCenter().x - 32))
			{
				/*
				 * We need to detect whether the character has moved back to center
				 * and if not, dont allow map to move until then.
				 */
				//std::cout << "Player not centered!\n";
				//std::cout << "Player [at::center]: " << this->playerSprite.getPosition().x << "::" << this->fixedView.getCenter().x << "\n";
				this->playerSprite.move(4.f, 0.f);
				this->updatePlayerMovement('x');
			}
			else {
				/*
				 * We can continue to move the view to the right
				 */
				this->fixedView.move(4.f, 0.f);
				this->playerSprite.move(4.f, 0.f);
				this->updatePlayerMovement('x');
			}
		}

	}
	else if (direction == sf::Keyboard::A)
	{
		//
		//std::cout << "[LEFT][Center: " << this->fixedView.getCenter().x << "," << this->fixedView.getCenter().y << "][PlayerPOS: " << this->playerSprite.getPosition().x << "," << this->playerSprite.getPosition().y << "]\n";

		if ((this->playerSprite.getScale().x > 0) || (this->playerSprite.getRotation() == 180))
		{
			this->updatePlayerScale('x', 'L');
		}

		//std::cout << "Player LEFT: " << this->playerSprite.getPosition().x << "\n";
		if (this->fixedView.getCenter().x <= 256)
		{
			std::cout << "[LEFT] Unable to move any further!\n";
			if ((this->playerSprite.getPosition().x - 64) <= 0)
			{
				//
				std::cout << "Player can not move any further!\n";
			}
			else {
				//
				this->playerSprite.move(-4.f, 0.f);
				this->updatePlayerMovement('x');
			}
		}
		else {
			if (this->playerSprite.getPosition().x != (this->fixedView.getCenter().x + 32))
			{
				//
				this->playerSprite.move(-4.f, 0.f);
				this->updatePlayerMovement('x');
			}
			else {
				this->fixedView.move(-4.f, 0.f);
				this->playerSprite.move(-4.f, 0.f);
				this->updatePlayerMovement('x');
			}
		}
	}
	else if (direction == sf::Keyboard::W)
	{
		//
		if (this->playerSprite.getRotation() == 180)
		{
			this->updatePlayerScale('y', 'U');
		}

		if (this->fixedView.getCenter().y <= 128)
		{
			//std::cout << "[UP] Unable to move any further! [" << this->fixedView.getCenter().y << "]\n";
			if (this->playerSprite.getPosition().y <= 0)
			{
				std::cout << "Player cant move any further! [" << this->playerSprite.getPosition().y << "::" << this->fixedView.getCenter().y << "]\n";
			}
			else {
				this->playerSprite.move(0.f, -4.f);
				this->updatePlayerMovement('y');
			}
		}
		else {
			if (this->playerSprite.getPosition().y != (this->fixedView.getCenter().y - 32))
			{
				//std::cout << "Player not centered!\n";
				//std::cout << "Player [at::center]: " << this->playerSprite.getPosition().y << "::" << this->fixedView.getCenter().y << "\n";
				this->playerSprite.move(0.f, -4.f);
				this->updatePlayerMovement('y');
			}
			else {
				//std::cout << "[UP] Map + Player! [::" << this->playerSprite.getPosition().y << "]\n";
				this->fixedView.move(0.f, -4.f);
				this->playerSprite.move(0.f, -4.f);
				this->updatePlayerMovement('y');
			}
		}
	}
	else if (direction == sf::Keyboard::S)
	{
		//

		if (this->playerSprite.getRotation() == 0)
		{
			this->updatePlayerScale('y', 'D');
		}

		//std::cout << "[DOWN] Y Player: " << this->playerSprite.getPosition().y << " Center: " << this->fixedView.getCenter().y << "\n";

		if (this->fixedView.getCenter().y >= 384)
		{
			//std::cout << "[DOWN] Unable to move any further! [" << this->fixedView.getCenter().y << "]\n";
			if (this->playerSprite.getPosition().y >= 512)
			{
				std::cout << "Player cant move any further! [" << this->playerSprite.getPosition().y << "::" << this->fixedView.getCenter().y << "]\n";
			}
			else {
				this->playerSprite.move(0.f, 4.f);
				this->updatePlayerMovement('y');
			}
		}
		else {
			if (this->playerSprite.getPosition().y != (this->fixedView.getCenter().y + 32))
			{
				//std::cout << "Player not centered!\n";
				//std::cout << "Player [at::center]: " << this->playerSprite.getPosition().y << "::" << this->fixedView.getCenter().y << "\n";
				this->playerSprite.move(0.f, 4.f);
				this->updatePlayerMovement('y');
			}
			else {
				//std::cout << "[DOWN] Map + Player! [::" << this->playerSprite.getPosition().y << "]\n";
				this->fixedView.move(0.f, 4.f);
				this->playerSprite.move(0.f, 4.f);
				this->updatePlayerMovement('y');
			}
		}
	}
}

void Game::player_move_stop(sf::Keyboard::Key direction)
{
	/*
	 * We stoppin!
	 *
	 * Reset animations, and insure standing animation is right
	 */
	if (this->ev.key.code == sf::Keyboard::D)
	{
		// Right
		this->playerLastSpriteX = 192;
		this->playerSprite.setTextureRect(sf::IntRect(0, 0, 64, 64));
		this->renderPlayer(*this->window);
	}
	else if (this->ev.key.code == sf::Keyboard::A)
	{
		// Left
		this->playerLastSpriteX = 192;
		this->playerSprite.setTextureRect(sf::IntRect(0, 0, 64, 64));
		this->renderPlayer(*this->window);
	}
	else if (this->ev.key.code == sf::Keyboard::W)
	{
		// Up
		this->playerLastSpriteY = 320;
		this->playerSprite.setTextureRect(sf::IntRect(0, 0, 64, 64));
		this->renderPlayer(*this->window);
	}
	else if (this->ev.key.code == sf::Keyboard::S)
	{
		// Down
		this->playerLastSpriteY = 320;
		this->playerSprite.setRotation(0.f);
		this->playerSprite.setTextureRect(sf::IntRect(0, 0, 64, 64));
		this->playerSprite.setPosition(this->playerSprite.getPosition().x - 64, this->playerSprite.getPosition().y - 64);
		this->renderPlayer(*this->window);
	}

}
void Game::updatePlayerMovement(char axis)
{
	// Update the player sprice on movement

	// Only update the frame ever 0.2 seconds
	// Likely need to update this to take game's refresh into account instead
	if (this->clock.getElapsedTime().asSeconds() > 0.2f)
	{
		if (axis == 'x')
		{
			// We're moving along the X axis
			if (this->playerLastSpriteX == 192)
			{
				this->playerSprite.setTextureRect(sf::IntRect(64, 0, 64, 64));
				this->playerLastSpriteX = 64;
			}
			else if (this->playerLastSpriteX == 64)
			{
				this->playerSprite.setTextureRect(sf::IntRect(128, 0, 64, 64));
				this->playerLastSpriteX = 128;
			}
			else if (this->playerLastSpriteX == 128)
			{
				this->playerSprite.setTextureRect(sf::IntRect(192, 0, 64, 64));
				this->playerLastSpriteX = 192;
			}
		}
		else if (axis == 'y')
		{
			// We're moving along the Y axis
			if (this->playerLastSpriteY == 256)
			{
				this->playerSprite.setTextureRect(sf::IntRect(320, 0, 64, 64));
				this->playerLastSpriteY = 320;
			}
			else if (this->playerLastSpriteY == 320)
			{
				this->playerSprite.setTextureRect(sf::IntRect(256, 0, 64, 64));
				this->playerLastSpriteY = 256;
			}
		}

		this->clock.restart();
		this->renderPlayer(*this->window);
	}
}
void Game::updatePlayerScale(char axis, char direction)
{
	// Insure the character sprite is looking at the right direction
	if (axis == 'x')
	{
		if (direction == 'R')
		{
			// Going Right!
			// 
			// Set the character to look right
			this->playerSprite.setRotation(0.f);
			this->playerSprite.setScale(1.f, 1.f);
			// Now that we're flipped, adjust position to make up for the movement scaling causes
			this->playerSprite.setPosition(this->playerSprite.getPosition().x - 64.f, this->playerSprite.getPosition().y);
		}
		else if (direction == 'L')
		{
			// Going Left!
			this->playerSprite.setRotation(0.f);
			this->playerSprite.setScale(-1.f, 1.f);
			this->playerSprite.setPosition(this->playerSprite.getPosition().x + 64.f, this->playerSprite.getPosition().y);
		}
	}
	else if (axis == 'y')
	{
		if (direction == 'U')
		{
			// Going Up!
			//this->playerSprite.setScale(1.f, 1.f);
			this->playerSprite.setRotation(0.f);
			this->playerSprite.setPosition(this->playerSprite.getPosition().x, this->playerSprite.getPosition().y);
		}
		else if (direction == 'D')
		{
			// Going Down!
			if (this->playerSprite.getScale().x == -1.f)
			{
				// We are looking left so no need to add 64 to X
				this->playerSprite.setScale(1.f, 1.f);
				this->playerSprite.setRotation(180.f);
				this->playerSprite.setPosition(this->playerSprite.getPosition().x, this->playerSprite.getPosition().y + 64);
			}
			else {
				// any other direction need to add 64 to X
				this->playerSprite.setScale(1.f, 1.f);
				this->playerSprite.setRotation(180.f);
				this->playerSprite.setPosition(this->playerSprite.getPosition().x + 64, this->playerSprite.getPosition().y + 64);
			}
		}
	}
}
void Game::player_attack()
{
	// We be attackin somethin
	std::cout << "Attack!\n";

	// Set low chance of a miss
	// int type = rand() % 100;

	// TWO animations need to happen here
	// 1. Player raising up gun
	// 2. Projectile start moving

	this->player_attack_anim(true);
	this->gameSounds.play_gunfire();
	this->player_projectile_anim(true);
	
	// Health deduction on target
	// this needs to be moved or delayed so it happens on hit, instead of on fire
	// likely where ever this goes, the hit sound effect should go with it
	this->hpbar_enemy(hpAdjustEnum::hit, 10);

	// We just attacked, so close attack window
	this->playerAttackWindow = false;
	// Reset the timer for another window
	this->timerBarPlayerTimeInt = 0;
	this->timerBarPlayerTime = sf::seconds(0.0f);
	//this->timerBarPlayerTimeLast = this->clock.getElapsedTime();
	this->timerBarPlayerTimeLast = sf::seconds(0);
	this->timerbar_player(timerBarPlayerEnum::restart, 0);

}
void Game::player_attack_anim(bool start)
{
	// Handle the animation of the player attack

	// We're starting the attack
	if (start)
	{
		this->playerSprite.setTextureRect(sf::IntRect(384, 0, 64, 64));
	} else {
		// We're checking when to end the attack frame
		if ((this->clock.getElapsedTime().asSeconds() - this->lastTime.asSeconds()) > 0.23f)
		{
			// reset back to standing frame
			this->playerSprite.setTextureRect(sf::IntRect(0, 0, 64, 64));
		}
	}
}
void Game::player_projectile_anim(bool start)
{
	// Handle animation for player's projectile
	if (start)
	{
		this->playerShotActive = true;
		this->playerAmmoSprite.setTextureRect(sf::IntRect(0, 0, 16, 16));
		this->playerAmmoSprite.setPosition(this->playerProjectileStartPOS);
	} else {
		// keep the sprite moving
		if ((this->clock.getElapsedTime().asMilliseconds() - this->lastProjectileTime.asMilliseconds()) > 1.0f)
		{
			this->playerProjectileLastPOS.x = this->playerProjectileLastPOS.x - 2;
			this->playerAmmoSprite.setPosition(this->playerProjectileLastPOS);
			this->lastProjectileTime = this->clock.getElapsedTime();
		}
	}

}
bool Game::player_projectileExplode_anim(bool start)
{
	// Handle animation for player's projectile
	if ((this->clock.getElapsedTime().asSeconds() - this->lastProjectileTime.asSeconds()) > 0.3f)
	{
		//std::cout << "Projectile Explode Hold\n";
		this->playerAmmoSprite.setPosition(this->playerProjectileLastPOS);
		return false;
	} else {
		//std::cout << "Projectile Explode\n";
		
		this->playerAmmoSprite.setTextureRect(sf::IntRect(16, 0, 16, 16));
		this->playerAmmoSprite.setPosition(this->playerProjectileLastPOS);
		return true;
	}

}