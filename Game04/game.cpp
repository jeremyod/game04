/*
 * Main game stuff
 * 
 * 
 */
#include "game.h"

void Game::initVariables()
{
	this->window = nullptr;
	this->backgroundToggle = 1;

	this->inCombat = false;

	// Work on adding dynamic keybindings
	// https://github.com/SFML/SFML/wiki/Tutorial:-Manage-dynamic-key-binding
	// https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1Keyboard.php#details
	//this->Up.isKeyPressed = sf::Keyboard::W;

}
void Game::initWindow()
{
	//this->videoMode.width = 512;
	//this->videoMode.height = 256;
	this->videoMode.width = 960;
	this->videoMode.height = 540;

	//this->viewportSize = sf::FloatRect(0, 0, 1920, 1080);
	this->viewportSize = sf::FloatRect(0, 0, 512, 256);

	this->window = new sf::RenderWindow(this->videoMode, "First Game", sf::Style::Default);

	this->window->setFramerateLimit(60);

	this->fixedView = sf::View(this->viewportSize);
	this->fixedView.setViewport(sf::FloatRect(0, 0, 1, 1));
}
void Game::initText()
{
	//
	if (!this->menuFont.loadFromFile("res/fonts/LiberationMono-Bold.ttf"))
	{
		std::cout << "Error loading Font\n";
	}

	this->combatMenuAttack.setFont(menuFont);
	this->combatMenuSpecial.setFont(menuFont);
	this->combatMenuItem.setFont(menuFont);

	// Char size is pixels
	this->combatMenuAttack.setCharacterSize(8);
	this->combatMenuSpecial.setCharacterSize(8);
	this->combatMenuItem.setCharacterSize(8);

	// Attack set to Yellow as its the default selection
	this->combatMenuAttack.setFillColor(sf::Color::Yellow);
	this->combatMenuSpecial.setFillColor(sf::Color::White);
	this->combatMenuItem.setFillColor(sf::Color::White);

	// The default selection is Attack
	this->combatMenuSelect = combatMenuEnum::attack;

	// Special attacks menu
	this->specialMenuSpark.setFont(menuFont);
	this->specialMenuMetal.setFont(menuFont);
	this->specialMenuStar.setFont(menuFont);

	this->specialMenuSpark.setCharacterSize(8);
	this->specialMenuMetal.setCharacterSize(8);
	this->specialMenuStar.setCharacterSize(8);

	this->specialMenuSpark.setFillColor(sf::Color::Yellow);
	this->specialMenuMetal.setFillColor(sf::Color::White);
	this->specialMenuStar.setFillColor(sf::Color::White);

	this->itemMenuETank.setFont(menuFont);
	this->itemMenuSPTank.setFont(menuFont);

	this->itemMenuETank.setCharacterSize(8);
	this->itemMenuSPTank.setCharacterSize(8);

	this->itemMenuETank.setFillColor(sf::Color::Yellow);
	this->itemMenuSPTank.setFillColor(sf::Color::White);

	this->showMenuSpecial = false;
	this->showMenuItem = false;
}
void Game::initPlayer()
{
	this->playerHealth = 100;
	this->playerAttack = false;

	this->playerTexture.loadFromFile("res/megaman3.png");

	this->playerSprite.setTexture(this->playerTexture);
	this->playerSprite.setTextureRect(sf::IntRect(0, 0, 64, 64));
	sf::Vector2f getCenter = this->fixedView.getCenter();
	this->playerSprite.setPosition(getCenter.x - 32, getCenter.y - 32);

	this->playerLastSpriteX = 192;
	this->playerLastSpriteY = 320;
}
void Game::initPlayerAmmo()
{
	// Player ammo
	this->playerAmmoTexture.loadFromFile("res/player-bullet-sheet.png");
	this->playerAmmoSprite.setTexture(this->playerAmmoTexture);
	this->playerAmmoSprite.setTextureRect(sf::IntRect(0, 0, 16, 16));
	this->playerShotActive = false;

	this->playerProjectileStartPOS = sf::Vector2f(375, 181);
	this->playerProjectileLastPOS = sf::Vector2f(375, 181);
}

Game::Game()
{
	this->initVariables();
	this->initWindow();
	this->initPlayer();
	this->initEnemy();
	this->initText();
}

Game::~Game()
{
	delete this->window;
}
const bool Game::running() const
{
	return this->window->isOpen();
}
const bool Game::getEndGame() const
{
	return false;
}
void Game::pollEvents()
{
	//
	while (this->window->pollEvent(this->ev))
	{
		//
		switch (this->ev.type)
		{
		case sf::Event::Closed:
			this->window->close();
			break;
		case sf::Event::KeyPressed:
			if (this->ev.key.code == sf::Keyboard::Escape)
			{
				this->window->close();
			}

			if (this->ev.key.code == sf::Keyboard::B)
			{
				// Manually trigger an encounter
				this->enemy_encounter_start();
			}

			if ((this->ev.key.code == sf::Keyboard::D) || (this->ev.key.code == sf::Keyboard::A) || (this->ev.key.code == sf::Keyboard::W) || (this->ev.key.code == sf::Keyboard::S))
			{
				// We're moving somewhere!
				if (!this->inCombat)
				{
					// If we're not in combat, move us around
					this->player_move(this->ev.key.code);

					// check for random encounter chance here
					//int type = rand() % 55;
					//int randEncounter = rand();
					/*
					if (randEncounter == 24)
					{
						// chance of hitting an encounter
						std::cout << "**Random Chance hit!**\n";
						this->enemy_encounter_start();
					}
					*/
				}

			}

			if ((this->ev.key.code == sf::Keyboard::Space) && (!this->playerShotActive))
			{
				// Fire stuff
				if ((this->inCombat) && (this->playerAttackWindow))
				{
					if (this->combatMenuSelect == combatMenuEnum::attack)
					{
						this->lastTime = this->clock.getElapsedTime();
						this->lastProjectileTime = this->clock.getElapsedTime();
						this->initPlayerAmmo();
						this->player_attack();
					}
					else if (this->combatMenuSelect == combatMenuEnum::special)
					{
						// Invoke the selected special [attack]
						std::cout << "Special Menu Selected\n";
					}
					else if (this->combatMenuSelect == combatMenuEnum::item)
					{
						// Invoke the selected game item [heals, etc]
						std::cout << "Item Menu Selected\n";
						
					}
				}
			}
			

			if ((this->ev.key.code == sf::Keyboard::Up) || (this->ev.key.code == sf::Keyboard::Down))
			{
				// Menu selection while in combat
				if (this->inCombat)
				{
					// need to work on direction
					if (this->showMenuSpecial)
					{
						this->special_menu_select(this->ev.key.code);
					}
					else if (this->showMenuItem)
					{
						this->item_menu_select(this->ev.key.code);
					}
					else {
						this->combat_menu_select(this->ev.key.code);
					}
				}
			}

			if (this->ev.key.code == sf::Keyboard::Right)
			{
				if (this->inCombat)
				{
					// Go into a submenu while in combat
					// If Special or Item options are selecterd
					if (this->combatMenuSelect == combatMenuEnum::special)
					{
						// 
						this->special_menu_player();
						this->showMenuSpecial = true;
					}
					else if (this->combatMenuSelect == combatMenuEnum::item)
					{
						this->item_menu_player();
						this->showMenuItem = true;
					}
				}
			}
			if (this->ev.key.code == sf::Keyboard::Left)
			{
				// Go back to previous menu while in combat
				if (this->inCombat)
				{
					if (this->combatMenuSelect == combatMenuEnum::special)
					{
						this->showMenuSpecial = false;
					}
					else if (this->combatMenuSelect == combatMenuEnum::item)
					{
						this->showMenuItem = false;
					}
				}
			}
			
			break; // End sf::Event::KeyPressed

		case sf::Event::KeyReleased:
			if ((this->ev.key.code == sf::Keyboard::D) || (this->ev.key.code == sf::Keyboard::A) || (this->ev.key.code == sf::Keyboard::W) || (this->ev.key.code == sf::Keyboard::S))
			{
				if (!this->inCombat)
				{
					// We're stopping somewhere!
					this->player_move_stop(this->ev.key.code);
				}
			}
			break;

		}
	}
}

void Game::enemy_encounter_start()
{
	// Start enemy encounter
	std::cout << "-- Start enemy encounter --\n";

	// We are now in combat mode 
	this->inCombat = true;

	this->transitionCascade();

	// Last position of player on world map
	// so we can place them back after encounter
	this->playerLastPos = this->playerSprite.getPosition();

	//this->fixedView.reset(sf::FloatRect(0, 0, 512, 256));
	this->fixedView.reset(this->viewportSize);
	// Draw player to one side
	this->playerSprite.setTextureRect(sf::IntRect(0, 0, 64, 64));
	// Insure rotation and scale are set for looking left
	this->playerSprite.setRotation(0.f);
	this->playerSprite.setScale(-1.f, 1.f);
	this->playerSprite.setPosition(448, 160);

	// Draw enemy to other side
	this->enemySprite.setPosition(64, 125);

	// Draw menu system
	this->combat_menu_player();

	this->hpbar_player();
	this->hpbar_enemy(hpAdjustEnum::init,100);

	this->timerbar_player(timerBarPlayerEnum::init,0);
	this->timerbar_enemy(timerBarPlayerEnum::init, 0);

	//this->timerBarPlayerTimeLast  = this->clock.getElapsedTime();
	this->timerBarPlayerTimeLast = sf::seconds(0);
	this->timerBarEnemyTimeLast = this->clock.getElapsedTime();

	this->pollTimeTick = this->clock.getElapsedTime();
}


void Game::pollCombatEvents()
{
	// We're in combat so lets poll those events


	if (int(this->clock.getElapsedTime().asSeconds() - this->pollTimeTick.asSeconds()) == 0)
	{
		// This section checks every 1 whole second.
		// -----------------------------------------

		// Player Attack Window Check
		//std::cout << "[Clock: " << this->clock.getElapsedTime().asSeconds() << "] [Last: " << this->timerBarPlayerTimeLast.asSeconds() << "]\n";
		std::cout << "[TimeLast]: " << this->timerBarPlayerTimeLast.asSeconds() << "\n";
		if (this->timerBarPlayerTimeLast < sf::seconds(30))
		{
			std::cout << "[Player] Attack window False\n";
			this->playerAttackWindow = false;
			timerbar_player(timerBarPlayerEnum::tick, 1);
			this->timerBarPlayerTimeLast = this->timerBarPlayerTimeLast + sf::seconds(1);
		}
		else {
			std::cout << "[Player] Attack Window True\n";
			timerbar_player(timerBarPlayerEnum::ready, 0);
			this->playerAttackWindow = true;
		}

		// Enemy Attack Window Check
		if ((this->clock.getElapsedTime().asSeconds() - this->timerBarEnemyTimeLast.asSeconds()) < 45.0f)
		{
			//std::cout << "[Enemy] Attack window False\n";
			this->enemyAttackWindow = false;
			timerbar_enemy(timerBarPlayerEnum::tick, 1);
		} else {
			std::cout << "[Enemy] Attack Window True\n";
			this->enemyAttackWindow = true;
			// attack! 
			std::cout << "Enemy Attack!\n";
			this->lastEnemyProjectileTime = this->clock.getElapsedTime();
			this->enemy_attack();
		}


		// -------------------------------------------------------
		// +1 poll time tick to insure that if always ends up as 0
		this->pollTimeTick = this->pollTimeTick + sf::seconds(1);
	}
 
	this->player_attack_anim(false);
	this->enemy_attack_anim(false);

	if (this->playerShotActive)
	{
		// Projectile in flight
		sf::FloatRect EnemySpriteAdjust;

		EnemySpriteAdjust = this->enemySprite.getGlobalBounds();
		// attacking right side, so use "width" instead of "left"
		EnemySpriteAdjust.width = EnemySpriteAdjust.width - 67;

		if (this->playerAmmoSprite.getGlobalBounds().intersects(EnemySpriteAdjust))
		{
			// Ammo Intersects with enemy
			this->gameSounds.play_projectilehit();
			this->playerShotActive = this->player_projectileExplode_anim(true);
		}
		else {
			this->player_projectile_anim(false);
		}
	}
	if (this->enemyShotActive)
	{
		sf::FloatRect playerSpriteAdjust;

		playerSpriteAdjust = this->playerSprite.getGlobalBounds();
		// since we're hitting the left side of the player, need to use "left" here instead of width
		playerSpriteAdjust.left = this->playerSprite.getGlobalBounds().left + 24;


		if (this->enemyAmmoSprite.getGlobalBounds().intersects(playerSpriteAdjust))
		{
			this->enemyShotActive = this->enemy_projectileExplode_anim(true);
		}
		else {
			this->enemy_projectile_anim(false);
		}
	}

}
void Game::update()
{
	this->pollEvents();

	if (this->inCombat)
	{
		// We're in combat mode, so start polling combat events
		this->pollCombatEvents();
	}
}