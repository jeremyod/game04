#include "game.h"

/*

	Rendering of stuff

 */


void Game::render()
{
	this->window->clear();

	this->window->setView(this->fixedView);

	// We are not in combat, so display over world view stuff
	if (!this->inCombat)
	{
		this->renderBackground(*this->window);
		this->renderPlayer(*this->window);
	}
	else {
		// in combat, render the right background
		this->renderBackground2(*this->window);
		this->renderPlayer(*this->window);
		this->renderEnemy(*this->window);
        this->renderMenus(*this->window);
        this->renderHPBars(*this->window);
        this->renderTimerBarPlayer(*this->window);
        this->renderTimerBarEnemy(*this->window);
		if (this->playerShotActive)
		{
			this->renderPlayerAmmo(*this->window);
		}
        if (this->enemyShotActive)
        {
            this->renderEnemyAmmo(*this->window);
        }
        if (this->showMenuSpecial)
        {
            this->renderMenuSpecial(*this->window);
        }
        if (this->showMenuItem)
        {
            this->renderMenuItem(*this->window);
        }
	}

	this->window->display();
}

void Game::renderBackground(sf::RenderTarget& target)
{
	// Create tilemap background
	const int level[] =
	{
		0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0,
		1, 1, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3,
		0, 1, 0, 0, 2, 0, 3, 3, 3, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 2, 0, 3, 3, 3, 0, 1, 1, 1, 0, 0, 0,
		0, 1, 1, 0, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 1, 0, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2, 0, 0,
		0, 0, 1, 0, 3, 0, 2, 2, 0, 0, 1, 1, 1, 1, 2, 0, 0, 0, 1, 0, 3, 0, 2, 2, 0, 0, 1, 1, 1, 1, 2, 0,
		2, 0, 1, 0, 3, 0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1, 2, 0, 1, 0, 3, 0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1,
		0, 0, 1, 0, 3, 2, 2, 2, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 3, 2, 2, 2, 0, 0, 0, 0, 1, 1, 1, 1,
		0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0,
		1, 1, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3,
		0, 1, 0, 0, 2, 0, 3, 3, 3, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 2, 0, 3, 3, 3, 0, 1, 1, 1, 0, 0, 0,
		0, 1, 1, 0, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 1, 0, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2, 0, 0,
		0, 0, 1, 0, 3, 0, 2, 2, 0, 0, 1, 1, 1, 1, 2, 0, 0, 0, 1, 0, 3, 0, 2, 2, 0, 0, 1, 1, 1, 1, 2, 0,
		2, 0, 1, 0, 3, 0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1, 2, 0, 1, 0, 3, 0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1,
		0, 0, 1, 0, 3, 2, 2, 2, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 3, 2, 2, 2, 0, 0, 0, 0, 1, 1, 1, 1,
	};

	// 512x256 (32x32; 16,8)
	// 1024x512 (32x32; 32,16)
	this->background.load("res/tileset.png", sf::Vector2u(32, 32), level, 32, 16);
	backgroundTexture = this->background.getTexture();
	// this only returns size of "tileset.png" file
	//this->backgroundSize = backgroundTexture.getSize();
	target.draw(this->background.getVertices(), &backgroundTexture);
}
void Game::renderBackground2(sf::RenderTarget& target)
{
	const int level[] =
	{
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

	};

	this->background2.load("res/tileset.png", sf::Vector2u(32, 32), level, 32, 16);
	backgroundTexture2 = this->background2.getTexture();
	target.draw(this->background2.getVertices(), &backgroundTexture2);
}
void Game::renderPlayerAmmo(sf::RenderTarget& target)
{
	target.draw(this->playerAmmoSprite);
}
void Game::renderPlayer(sf::RenderTarget& target)
{
	target.draw(this->playerSprite);
}
void Game::renderEnemyAmmo(sf::RenderTarget& target)
{
    target.draw(this->enemyAmmoSprite);
}
void Game::renderEnemy(sf::RenderTarget& target)
{
	target.draw(this->enemySprite);
}
void Game::renderMenus(sf::RenderTarget& target)
{
    target.draw(this->menuBackground);
    target.draw(this->combatMenuAttack);
    target.draw(this->combatMenuSpecial);
    target.draw(this->combatMenuItem);
}
void Game::renderMenuSpecial(sf::RenderTarget& target)
{
    target.draw(this->specialMenuBackground);
    target.draw(this->specialMenuSpark);
    target.draw(this->specialMenuMetal);
    target.draw(this->specialMenuStar);
}
void Game::renderMenuItem(sf::RenderTarget& target)
{
    target.draw(this->itemMenuBackground);
    target.draw(this->itemMenuETank);
    target.draw(this->itemMenuSPTank);
}
void Game::renderHPBars(sf::RenderTarget& target)
{
    target.draw(this->playerHPBar);
    target.draw(this->enemyHPBar);
}
void Game::renderTimerBarPlayer(sf::RenderTarget& target)
{
    target.draw(this->timerBarPlayer);
}
void Game::renderTimerBarEnemy(sf::RenderTarget& target)
{
    target.draw(this->timerBarEnemy);
}

void Game::transitionCascade()
{

    // The overall width & height of the layout
    int width, height;

    // Iteration counters
    int iter, w, h;
    int ictw, ccnew, endrows;
    int c, ht, wt;

    int ictww, cc;
    bool newrow;
    int ctotal;

    // Memory position counter for vertices
    int quadMem;

    // the tileset we're using
    //sf::Texture m_tileset;
    // Storing all the verticies
    sf::VertexArray m_vertices;

    // Layout of the level


    // 0 = grass
    // 1 = water
    const int level[] =
    {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

    };

    // level
    width = 30;
    height = 18;

    // Iterations is the amount of columns (width) plus height (rows)
    iter = width + height;

    w = 1; h = 1; // Start these counters at 1
    ictw = width; // count the widths
    endrows = height - 1;
    ccnew = 0;

    quadMem = 0;
    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(width * height * 4);

    sf::Texture m_tileset;
    m_tileset = this->background.getTexture();


    ctotal = 0;

    for (int ic = 0; ic < iter; ic++)
    {
        // ic = Iteration Count
        if (ic <= width)
        {
            // Iteration Count is less or equal to width
            // This means we're still doing increasing angular columns
            for (int wc = 0; wc < width; wc++)
            {
                //    wc = Width Count
                // width = columns 
                // Iterate through how many columns we have
                if (w > 1)
                {
                    // We are beyond the first column.
                    // this means we're doing angular stuff now
                    if (w <= width)
                    {
                        c = 0;
                        ht = 1;
                        wt = w;

                        while (c < height)
                        {
                            //
                            if ((ht > 0) && (wt > 0))
                            {
                                sf::Vertex* quad = &m_vertices[quadMem];

                                this->quadPosition(quad, wt, ht);

                                int tileNumber = level[ctotal];

                                this->quadCoords(quad, tileNumber);

                                window->draw(m_vertices, &m_tileset);

                                ctotal++;
                                quadMem = quadMem + 4;
                            }
                            ht++; // go over to next height (row)
                            wt--; // go down to next width (column)

                            c++;
                        }
                        // Display the whole angular row at once.
                        // This is much faster than trying to render each cell at a time.
                        window->display();
                        w++;
                        //print("\n");
                        //std::cout << "\n";
                    }
                }
                else {
                    // Most top-left position, single cell in the grid.
                    // Very first position (0,0)
                    //std::cout << "[0,0; 32,0; 32,32; 0,32]\n";

                    sf::Vertex* quad = &m_vertices[quadMem];

                    //this->quadPosition(quad,0,0);

                    quad[0].position = sf::Vector2f(0, 0);
                    quad[1].position = sf::Vector2f(32, 0);
                    quad[2].position = sf::Vector2f(32, 32);
                    quad[3].position = sf::Vector2f(0, 32);

                    int tileNumber = level[ctotal];

                    this->quadCoords(quad, tileNumber);

                    ctotal++;
                    quadMem = quadMem + 4;
                    w++;

                    this->window->draw(m_vertices, &m_tileset);
                    this->window->display();
                }
            }
        }
        else {
            // We've maxed out on the width count, 
            // so we're going to start making smaller angular columns
            ictww = width;
            cc = 2;
            c = 0;
            newrow = true;
            /*
             * endrows starts at rows - 1
                then it needs to -1 for each subsequent loop
             */
            while (c < endrows)
            {
                if (newrow)
                {
                    newrow = false;
                    cc = cc + ccnew;
                }

                sf::Vertex* quad = &m_vertices[quadMem];

                this->quadPosition(quad, ictww, cc);

                int tileNumber = level[ctotal];

                this->quadCoords(quad, tileNumber);

                ictww--;
                cc++;
                c++;
                quadMem = quadMem + 4;
                ctotal++;

                window->draw(m_vertices, &m_tileset);
            }
            endrows--;
            ccnew++;
            window->display();
        }
    }
    //std::cout << "\n\n";
    //std::cout << "[w:" << w << "]\n";

}
void Game::quadPosition(sf::Vertex* quad, unsigned int wt, unsigned int ht)
{
    //
    quad[0].position = sf::Vector2f((wt - 1) * 32, (ht - 1) * 32);
    quad[1].position = sf::Vector2f(wt * 32, (ht - 1) * 32);
    quad[2].position = sf::Vector2f(wt * 32, ht * 32);
    quad[3].position = sf::Vector2f((wt - 1) * 32, ht * 32);

}
void Game::quadCoords(sf::Vertex* quad, int tileNumber)
{
    //
    unsigned int tileX = tileNumber % 8;
    unsigned int tileY = tileNumber / 8;

    quad[0].texCoords = sf::Vector2f(tileX * 32, tileY * 32);
    quad[1].texCoords = sf::Vector2f((tileX + 1) * 32, tileY * 32);
    quad[2].texCoords = sf::Vector2f((tileX + 1) * 32, (tileY + 1) * 32);
    quad[3].texCoords = sf::Vector2f(tileX * 32, (tileY + 1) * 32);
}