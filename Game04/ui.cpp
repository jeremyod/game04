
#include "game.h"


void Game::combat_menu_select(sf::Keyboard::Key direction)
{
	// Make a menu selection
	if (direction == sf::Keyboard::Down)
	{
		if (this->combatMenuSelect == combatMenuEnum::attack)
		{
			// Attack is selected, change to special
			this->combatMenuAttack.setFillColor(sf::Color::White);
			this->combatMenuSpecial.setFillColor(sf::Color::Yellow);
			this->combatMenuSelect = combatMenuEnum::special;
		}
		else if (this->combatMenuSelect == combatMenuEnum::special)
		{
			// special is selected, change to item
			this->combatMenuSpecial.setFillColor(sf::Color::White);
			this->combatMenuItem.setFillColor(sf::Color::Yellow);
			this->combatMenuSelect = combatMenuEnum::item;
		}
		else if (this->combatMenuSelect == combatMenuEnum::item)
		{
			// item is selected, change to attack
			this->combatMenuItem.setFillColor(sf::Color::White);
			this->combatMenuAttack.setFillColor(sf::Color::Yellow);
			this->combatMenuSelect = combatMenuEnum::attack;
		}
	}

	if (direction == sf::Keyboard::Up)
	{
		if (this->combatMenuAttack.getFillColor() == sf::Color::Yellow)
		{
			// Attack is selected, change to item
			this->combatMenuAttack.setFillColor(sf::Color::White);
			this->combatMenuItem.setFillColor(sf::Color::Yellow);
			this->combatMenuSelect = combatMenuEnum::item;
		}
		else if (this->combatMenuSpecial.getFillColor() == sf::Color::Yellow)
		{
			// special is selected, change to Attack
			this->combatMenuSpecial.setFillColor(sf::Color::White);
			this->combatMenuAttack.setFillColor(sf::Color::Yellow);
			this->combatMenuSelect = combatMenuEnum::attack;
		}
		else if (this->combatMenuItem.getFillColor() == sf::Color::Yellow)
		{
			// item is selected, change to Special
			this->combatMenuItem.setFillColor(sf::Color::White);
			this->combatMenuSpecial.setFillColor(sf::Color::Yellow);
			this->combatMenuSelect = combatMenuEnum::special;
		}
	}
	this->gameSounds.play_menu_select();
}
void Game::special_menu_select(sf::Keyboard::Key direction)
{
	if (direction == sf::Keyboard::Down)
	{
		if (this->specialMenuSelect == specialMenuEnum::spark)
		{
			// Spark is selected, change to special
			this->specialMenuSpark.setFillColor(sf::Color::White);
			this->specialMenuMetal.setFillColor(sf::Color::Yellow);
			this->specialMenuSelect = specialMenuEnum::metal;
		}
		else if (this->specialMenuSelect == specialMenuEnum::metal)
		{
			// special is selected, change to item
			this->specialMenuMetal.setFillColor(sf::Color::White);
			this->specialMenuStar.setFillColor(sf::Color::Yellow);
			this->specialMenuSelect = specialMenuEnum::star;
		}
		else if (this->specialMenuSelect == specialMenuEnum::star)
		{
			// item is selected, change to attack
			this->specialMenuStar.setFillColor(sf::Color::White);
			this->specialMenuSpark.setFillColor(sf::Color::Yellow);
			this->specialMenuSelect = specialMenuEnum::spark;
		}
	}

	if (direction == sf::Keyboard::Up)
	{
		if (this->specialMenuSelect == specialMenuEnum::spark)
		{
			// Spark is selected, change to special
			this->specialMenuSpark.setFillColor(sf::Color::White);
			this->specialMenuStar.setFillColor(sf::Color::Yellow);
			this->specialMenuSelect = specialMenuEnum::star;
		}
		else if (this->specialMenuSelect == specialMenuEnum::metal)
		{
			// special is selected, change to item
			this->specialMenuMetal.setFillColor(sf::Color::White);
			this->specialMenuSpark.setFillColor(sf::Color::Yellow);
			this->specialMenuSelect = specialMenuEnum::spark;
		}
		else if (this->specialMenuSelect == specialMenuEnum::star)
		{
			// item is selected, change to attack
			this->specialMenuStar.setFillColor(sf::Color::White);
			this->specialMenuMetal.setFillColor(sf::Color::Yellow);
			this->specialMenuSelect = specialMenuEnum::metal;
		}
	}
	this->gameSounds.play_menu_select();
}
void Game::combat_menu_player()
{
	// Display a menu for the player to make selections from

	this->menuBackground.setPrimitiveType(sf::Quads);
	this->menuBackground.resize(16);

	// Shadowed backdrop
	sf::Vertex* backgroundShadowQuad = &this->menuBackground[4];
	this->draw_box(backgroundShadowQuad,400,80,43,38);
	this->color_box(backgroundShadowQuad, sf::Color::Black);

	// Actual menu
	sf::Vertex* backgroundQuad = &this->menuBackground[8];
	this->draw_box(backgroundQuad, 400,80, 40, 35);
	this->color_box(backgroundQuad, sf::Color::Blue);

	// Text in the box
	this->combatMenuAttack.setString("Attack");
	this->combatMenuSpecial.setString("Special");
	this->combatMenuItem.setString("Item");

	this->combatMenuAttack.setPosition(402, 82);
	this->combatMenuSpecial.setPosition(402, 92);
	this->combatMenuItem.setPosition(402, 102);

}
void Game::special_menu_player()
{
	// Special attacks menu options
	// specials will have uses that deplete an SP bar

	this->specialMenuBackground.setPrimitiveType(sf::Quads);
	this->specialMenuBackground.resize(16);

	sf::Vertex* backgroundShadowQuad = &this->specialMenuBackground[4];
	this->draw_box(backgroundShadowQuad,430,90,43,38);
	this->color_box(backgroundShadowQuad, sf::Color::Black);

	sf::Vertex* backgroundQuad = &this->specialMenuBackground[8];
	this->draw_box(backgroundQuad, 430, 90, 40, 35);
	this->color_box(backgroundQuad, sf::Color::Blue);

	/* Special Attack options
	 *  (currently megaman themed)
	 * - Spark
	 * - Metal
	 * - Star
	 */
	this->specialMenuSpark.setString("Spark");
	this->specialMenuMetal.setString("Metal");
	this->specialMenuStar.setString("Star");

	this->specialMenuSpark.setPosition(432,94);
	this->specialMenuMetal.setPosition(432, 104);
	this->specialMenuStar.setPosition(432, 114);

}
void Game::item_menu_player()
{
	// Item menu options
	// items will have a specific count of uses
	this->itemMenuBackground.setPrimitiveType(sf::Quads);
	this->itemMenuBackground.resize(16);

	this->itemMenuBackground.setPrimitiveType(sf::Quads);
	this->itemMenuBackground.resize(16);

	sf::Vertex* backgroundShadowQuad = &this->itemMenuBackground[4];
	this->draw_box(backgroundShadowQuad, 430, 90, 43, 38);
	this->color_box(backgroundShadowQuad, sf::Color::Black);

	sf::Vertex* backgroundQuad = &this->itemMenuBackground[8];
	this->draw_box(backgroundQuad, 430, 90, 40, 35);
	this->color_box(backgroundQuad, sf::Color::Blue);

	/* Item Options
	 * - Energy Tank
	 * - SP Tank
	 * 
	 */
	this->itemMenuETank.setString(" E-Tank");
	this->itemMenuSPTank.setString("SP-Tank");

	this->itemMenuETank.setPosition(432, 94);
	this->itemMenuSPTank.setPosition(432, 104);
}

void Game::item_menu_select(sf::Keyboard::Key direction)
{
	if (direction == sf::Keyboard::Down)
	{
		if (this->itemMenuSelect == itemMenuEnum::etank)
		{
			// Spark is selected, change to special
			this->itemMenuETank.setFillColor(sf::Color::White);
			this->itemMenuSPTank.setFillColor(sf::Color::Yellow);
			this->itemMenuSelect = itemMenuEnum::sptank;
		}
		else if (this->itemMenuSelect == itemMenuEnum::sptank)
		{
			// special is selected, change to item
			this->itemMenuSPTank.setFillColor(sf::Color::White);
			this->itemMenuETank.setFillColor(sf::Color::Yellow);
			this->itemMenuSelect = itemMenuEnum::etank;
		}
	}

	if (direction == sf::Keyboard::Up)
	{
		if (this->itemMenuSelect == itemMenuEnum::etank)
		{
			// Spark is selected, change to special
			this->itemMenuETank.setFillColor(sf::Color::White);
			this->itemMenuSPTank.setFillColor(sf::Color::Yellow);
			this->itemMenuSelect = itemMenuEnum::sptank;
		}
		else if (this->itemMenuSelect == itemMenuEnum::sptank)
		{
			// special is selected, change to item
			this->itemMenuSPTank.setFillColor(sf::Color::White);
			this->itemMenuETank.setFillColor(sf::Color::Yellow);
			this->itemMenuSelect = itemMenuEnum::etank;
		}
	}
	this->gameSounds.play_menu_select();
}

void Game::hpbar_player()
{
	// The HP Bar for the player
	this->playerHPBar.setPrimitiveType(sf::Quads);
	this->playerHPBar.resize(16);

	// Player POS (448, 160);
	sf::Vertex* playerHPBarBackground = &this->playerHPBar[4];
	this->draw_box(playerHPBarBackground, 395, 222, 50, 10);
	this->color_box(playerHPBarBackground, sf::Color::Black);

	sf::Vertex* playerHPBarForeground = &this->playerHPBar[8];
	this->draw_box(playerHPBarForeground, 397, 224, 46, 6);
	this->color_box(playerHPBarForeground, sf::Color::Green);

}
void Game::hpbar_enemy(Game::hpAdjustEnum type, int amt)
{
	if (type == hpAdjustEnum::init)
	{
		// Initialize the HP Bar.
		
		// HP Bar for the enemy
		this->enemyHPBar.setPrimitiveType(sf::Quads);
		this->enemyHPBar.resize(16);

		// Enemy POS (64, 125);
		sf::Vertex* enemyHPBarBackground = &this->enemyHPBar[4];
		this->draw_box(enemyHPBarBackground, 58, 222, 202, 10);
		this->color_box(enemyHPBarBackground, sf::Color::Black);

		sf::Vertex* enemyHPBarForeground = &this->enemyHPBar[8];
		this->draw_box(enemyHPBarForeground, 60, 224, this->enemyHealth, 6);
		this->color_box(enemyHPBarForeground, sf::Color::Green);
	}
	else if (type == hpAdjustEnum::hit)
	{
		// its a hit, reduce the hp bar by amt
		this->enemyHealth = this->enemyHealth - 20;
		sf::Vertex* enemyHPBarForeground = &this->enemyHPBar[8];
		this->draw_box(enemyHPBarForeground, 60, 224, this->enemyHealth, 6);
		this->color_box(enemyHPBarForeground, sf::Color::Green);
	}
}

void Game::timerbar_player(timerBarPlayerEnum type, int amt)
{
	// Time until player can act

	if (type == timerBarPlayerEnum::init)
	{
		this->timerBarPlayer.setPrimitiveType(sf::Quads);
		this->timerBarPlayer.resize(16);

		sf::Vertex* barBackground = &this->timerBarPlayer[4];
		this->draw_box(barBackground, 390, 235, 64, 8);
		this->color_box(barBackground, sf::Color::Black);

		sf::Vertex* barForeground = &this->timerBarPlayer[8];
		this->draw_box(barForeground, 392, 237, this->timerBarPlayerTimeInt, 4);
		this->color_box(barForeground, sf::Color::White);
	}
	else if (type == timerBarPlayerEnum::tick)
	{
		// Adjust the bar
		if (this->timerBarPlayerTimeInt < 30)
		{
			this->timerBarPlayerTimeInt = this->timerBarPlayerTimeInt + 1;
			sf::Vertex* barForeground = &this->timerBarPlayer[8];
			this->draw_box(barForeground, 392, 237, this->timerBarPlayerTimeInt, 4);
			this->color_box(barForeground, sf::Color::White);
		}
	}
	else if (type == timerBarPlayerEnum::ready)
	{
		// Timer bar is full
		sf::Vertex* barForeground = &this->timerBarPlayer[8];
		this->draw_box(barForeground, 392, 237, this->timerBarPlayerTimeInt, 4);
		this->color_box(barForeground, sf::Color::Red);
	}
	else if (type == timerBarPlayerEnum::restart)
	{
		// Combat turn completed, and we need to reset to 0
		sf::Vertex* barForeground = &this->timerBarPlayer[8];
		this->draw_box(barForeground, 392, 237, this->timerBarPlayerTimeInt, 4);
		this->color_box(barForeground, sf::Color::White);
	}

}

void Game::timerbar_enemy(timerBarPlayerEnum type, int amt)
{
	// Time until player can act

	if (type == timerBarPlayerEnum::init)
	{
		this->timerBarEnemy.setPrimitiveType(sf::Quads);
		this->timerBarEnemy.resize(16);

		sf::Vertex* barBackground = &this->timerBarEnemy[4];
		this->draw_box(barBackground, 90, 235, 64, 8);
		this->color_box(barBackground, sf::Color::Black);

		sf::Vertex* barForeground = &this->timerBarEnemy[8];
		this->draw_box(barForeground, 92, 237, this->timerBarEnemyTimeInt, 4);
		this->color_box(barForeground, sf::Color::White);
	} else if (type == timerBarPlayerEnum::tick)
	{
		// Deduct from the bar
		if (this->timerBarEnemyTimeInt > 0)
		{
			this->timerBarEnemyTimeInt = this->timerBarEnemyTimeInt - 1;
			sf::Vertex* barForeground = &this->timerBarEnemy[8];
			this->draw_box(barForeground, 92, 237, this->timerBarEnemyTimeInt, 4);
			this->color_box(barForeground, sf::Color::White);
		}
	}

}

void Game::draw_box(sf::Vertex* quad,int left, int top, int width, int height)
{
	// Generate the vector quad for a given size and position
	quad[0].position = sf::Vector2f(left,top);
	quad[1].position = sf::Vector2f(left + width,top);
	quad[2].position = sf::Vector2f(left + width, top + height);
	quad[3].position = sf::Vector2f(left, top + height);
}

void Game::color_box(sf::Vertex* quad, sf::Color color)
{
	quad[0].color = color;
	quad[1].color = color;
	quad[2].color = color;
	quad[3].color = color;
}