#pragma once
#include "stdafx.h"

class GameSound
{
	sf::SoundBuffer gunFireBuffer;
	sf::SoundBuffer projectileHitBuffer;
	sf::SoundBuffer menuSelectBuffer;

	sf::Sound gunFire;
	sf::Sound projectileHit;
	sf::Sound menuSelectSound;

	void soundInit();

	public:
		GameSound();
		void play_gunfire();
		void play_projectilehit();
		void play_menu_select();
};