#pragma once

#include "stdafx.h"
#include "tilemap.h"
#include "sound.h"

class Game
{
	// Main Stuff
	sf::RenderWindow* window;
	sf::VideoMode videoMode;
	sf::View view;
	sf::View fixedView;
	sf::Event ev;
	sf::FloatRect viewportSize;

	// Timing loop 
	sf::Time pollTimeTick;

	// Keybindings
	sf::Keyboard Up;
	sf::Keyboard Down;
	sf::Keyboard Left;
	sf::Keyboard Right;
	sf::Keyboard Fire;

	// Combat stuffs
	bool inCombat;
	bool playerAttack;
	int playerHealth;
	int enemyHealth;
	// player's last position
	sf::Vector2f playerLastPos;

	// UI Vars -----------------------
	sf::VertexArray menuBackground;
	sf::VertexArray playerHPBar;
	sf::VertexArray enemyHPBar;

	enum class hpAdjustEnum { init, hit, heal };
	hpAdjustEnum hpAdjustType;

	sf::Font menuFont;
	sf::Text combatMenuAttack;
	sf::Text combatMenuSpecial;
	sf::Text combatMenuItem;
	enum class combatMenuEnum { attack, special, item };
	combatMenuEnum combatMenuSelect;

	sf::VertexArray specialMenuBackground;
	sf::Text specialMenuSpark;
	sf::Text specialMenuMetal;
	sf::Text specialMenuStar;
	enum class specialMenuEnum { spark, metal, star };
	specialMenuEnum specialMenuSelect;
	bool showMenuSpecial;

	sf::VertexArray itemMenuBackground;
	sf::Text itemMenuETank;
	sf::Text itemMenuSPTank;
	enum class itemMenuEnum { etank, sptank };
	itemMenuEnum itemMenuSelect;
	bool showMenuItem;

	// Player Attack time
	sf::VertexArray timerBarPlayer;
	sf::Time timerBarPlayerTime;
	sf::Time timerBarPlayerTimeLast;
	int timerBarPlayerTimeInt;
	bool playerAttackWindow;
	enum class timerBarPlayerEnum { init, tick, restart, ready };

	// Enemy Attack Time
	sf::VertexArray timerBarEnemy;
	sf::Time timerBarEnemyTime;
	sf::Time timerBarEnemyTimeLast;
	int timerBarEnemyTimeInt;
	bool enemyAttackWindow;

	
	// Iniate Sounds object
	GameSound gameSounds;

	// Tilemap - background
	TileMap background;
	sf::Texture backgroundTexture;
	sf::Vector2u backgroundSize;
	// Alternate background
	TileMap background2;
	sf::Texture backgroundTexture2;
	sf::Vector2u backgroundSize2;
	int backgroundToggle;

	// Tilemap - player
	sf::Texture playerTexture;
	sf::Sprite playerSprite;
	sf::IntRect playerAnimFrame;
	int playerLastSpriteX;
	int playerLastSpriteY;

	sf::Texture playerAmmoTexture;
	sf::Sprite playerAmmoSprite;

	sf::Vector2f playerProjectileStartPOS;
	sf::Vector2f playerProjectileLastPOS;

	bool playerShotActive;


	// Enemy Sprite
	sf::Texture enemyTexture;
	sf::Sprite enemySprite;
	// Enemy Ammo
	sf::Texture enemyAmmoTexture;
	sf::Sprite enemyAmmoSprite;
	bool enemyShotActive;

	sf::Vector2f enemyProjectileStartPOS;
	sf::Vector2f enemyProjectileLastPOS;
	sf::Time lastEnemyProjectileTime;

	// For Player animation
	sf::Clock clock;
	sf::Time lastTime;
	sf::Time lastProjectileTime;

	void initVariables();
	void initWindow();
	void initPlayer();
	void initPlayerAmmo();
	void initEnemy();
	void initText();

	void pollEvents();

	void player_attack();
	void player_attack_anim(bool start);
	void player_projectile_anim(bool start);
	bool player_projectileExplode_anim(bool start);
	void enemy_encounter_start();

	void pollCombatEvents();

	// UI Functions -------------------------------------
	void combat_menu_player();
	void combat_menu_select(sf::Keyboard::Key direction);

	void special_menu_player();
	void special_menu_select(sf::Keyboard::Key direction);

	void item_menu_player();
	void item_menu_select(sf::Keyboard::Key direction);

	void hpbar_player();
	void hpbar_enemy(Game::hpAdjustEnum type, int amt);
	void draw_box(sf::Vertex* quad,int left, int top, int width, int height);
	void color_box(sf::Vertex* quad, sf::Color color);

	void timerbar_player(timerBarPlayerEnum type, int amt);
	void timerbar_enemy(timerBarPlayerEnum type, int amt);



	// Enemy Functions ----------------------------------
	void enemy_attack_roll();
	void enemy_attack();
	void enemy_attack_anim(bool start);
	void enemy_projectile_anim(bool start);
	bool enemy_projectileExplode_anim(bool start);

	// Player Functions ---------------------------------
	void player_move(sf::Keyboard::Key direction);
	void player_move_stop(sf::Keyboard::Key direction);
	void updatePlayerMovement(char axis);
	void updatePlayerScale(char axis, char direction);

	// Rendering ---------------------------------
	void renderBackground(sf::RenderTarget& target);
	void renderBackground2(sf::RenderTarget& target);
	void renderPlayerAmmo(sf::RenderTarget& target);
	void renderEnemyAmmo(sf::RenderTarget& target);
	void renderPlayer(sf::RenderTarget& target);
	void renderEnemy(sf::RenderTarget& target);
	void renderMenus(sf::RenderTarget& target);
	void renderMenuSpecial(sf::RenderTarget& target);
	void renderMenuItem(sf::RenderTarget& target);
	void renderHPBars(sf::RenderTarget& target);
	void renderTimerBarPlayer(sf::RenderTarget& target);
	void renderTimerBarEnemy(sf::RenderTarget& target);
	void transitionCascade();
	void quadPosition(sf::Vertex* quad, unsigned int wt, unsigned int ht);
	void quadCoords(sf::Vertex* quad, int tileNumber);

	public:
		Game();
		virtual ~Game();

		const bool running() const;
		const bool getEndGame() const;
		void update();
		void render();

};