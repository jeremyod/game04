#include "game.h"


void Game::initEnemy()
{
	this->enemyHealth = 200;

	this->enemyTexture.loadFromFile("res/wardroid.png");
	this->enemySprite.setTexture(this->enemyTexture);
	this->enemySprite.setTextureRect(sf::IntRect(0, 0, 128, 128));

	// Ammo Stuff
	this->enemyAmmoTexture.loadFromFile("res/enemy-bullet-sheet.png");
	this->enemyAmmoSprite.setTexture(this->enemyAmmoTexture);
	this->enemyAmmoSprite.setTextureRect(sf::IntRect(0, 0, 16, 16));
	this->enemyShotActive = false;

	this->enemyProjectileStartPOS = sf::Vector2f(154, 168);
	this->enemyProjectileLastPOS = sf::Vector2f(154, 168);

	this->timerBarEnemyTimeInt = 45;
	this->timerBarEnemyTime = sf::seconds(45.0f);
}
void Game::enemy_attack_roll()
{
	// Rolling for attack
	int chance = rand() % 800;
	if (chance == 5)
	{
		// chance of hitting an encounter
		std::cout << "Enemy Attack!\n";
		this->lastEnemyProjectileTime = this->clock.getElapsedTime();
		this->enemy_attack();
	}
}
void Game::enemy_attack()
{
	// Start enemy attack

	// Attack & Projectile Animations
	this->enemy_attack_anim(true);
	this->enemy_projectile_anim(true);

	this->enemyAttackWindow = false;
	this->timerBarEnemyTimeInt = 30;
	this->timerBarEnemyTime = sf::seconds(45.0f);
	this->timerBarEnemyTimeLast = this->clock.getElapsedTime();
	this->timerbar_enemy(timerBarPlayerEnum::init, 0);
}
void Game::enemy_attack_anim(bool start)
{
	if (start)
	{
		this->enemySprite.setTextureRect(sf::IntRect(128, 0, 128, 128));
	}
	else {
		// We're checking when to end the attack frame
		if ((this->clock.getElapsedTime().asSeconds() - this->lastTime.asSeconds()) > 0.23f)
		{
			// reset back to standing frame
			this->enemySprite.setTextureRect(sf::IntRect(0, 0, 128, 128));
		}
	}
}
void Game::enemy_projectile_anim(bool start)
{
	//std::cout << "[Enemy Projectile Anim]: [start:" << start << "]\n";
	if (start)
	{
		this->enemyShotActive = true;
		this->enemyAmmoSprite.setTextureRect(sf::IntRect(0, 0, 16, 16));
		this->enemyAmmoSprite.setPosition(this->enemyProjectileStartPOS);
	}
	else {
		// keep the sprite moving
		if ((this->clock.getElapsedTime().asMilliseconds() - this->lastEnemyProjectileTime.asMilliseconds()) > 1.0f)
		{
			this->enemyProjectileLastPOS.x = this->enemyProjectileLastPOS.x + 2;
			this->enemyAmmoSprite.setPosition(this->enemyProjectileLastPOS);
			this->lastEnemyProjectileTime = this->clock.getElapsedTime();
		}
	}
}
bool Game::enemy_projectileExplode_anim(bool start)
{
	// Handle animation for player's projectile
	if ((this->clock.getElapsedTime().asSeconds() - this->lastEnemyProjectileTime.asSeconds()) > 0.3f)
	{
		//std::cout << "Projectile Explode Hold\n";
		this->enemyAmmoSprite.setPosition(this->enemyProjectileLastPOS);
		//this->gameSounds.play_projectilehit();
		return false;
	} else {
		//std::cout << "Projectile Explode\n";
		this->enemyAmmoSprite.setTextureRect(sf::IntRect(16, 0, 16, 16));
		this->enemyAmmoSprite.setPosition(this->enemyProjectileLastPOS);
		return true;
	}

}